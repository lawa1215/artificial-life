use bevy::ecs::bundle::Bundle;
use crate::connectors;
use bevy::ecs::entity::Entity;
use bevy::ecs::world::World;

#[derive(Default)]
struct Prepend {
    prepend : Option<Entity>,
}

#[derive(Default)]
struct Append {
    append : Option<Entity>,
}

struct Protein;

struct GeneticTarget {
    target : &'static str,
}

#[derive(Bundle)]
pub struct Polymerase {
    prepend : Prepend,
    append  : Append,
    base_target : GeneticTarget,
    sugar_con : connectors::sugar::SugarConnector,
    acid_con : connectors::acid::AcidConnector,
    protein_tag : Protein,
}

pub fn create_polymerase(world : &mut World, target : &'static str) -> Entity {
    world.spawn().insert_bundle(Polymerase { prepend : Prepend::default(), append : Append::default(), 
        base_target : GeneticTarget{target : target}, 
        sugar_con : connectors::sugar::SugarConnector::default(),
        acid_con : connectors::acid::AcidConnector::default(),
        protein_tag : Protein
    }).id()
}

pub fn polymerase_target(world : &World, polymerase : Entity) -> Option<&str> {
    let target = world.get_entity(polymerase).unwrap().get::<GeneticTarget>()?;
    Some(target.target)
}

struct Specifier {
    target : &'static str,
}