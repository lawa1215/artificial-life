use bevy::ecs::entity::Entity;
use bevy::ecs::bundle::Bundle;
use bevy::ecs::world::World;

pub mod dna {
    use super::{Entity, World, Bundle};
    use crate::connectors;
    #[derive(Clone,Copy,PartialEq)]
    #[allow(unused)]
    pub enum NucleicAcid {
        A,
        C,
        T,
        G,
    }

    #[derive(Bundle)]
    pub struct Nucleotide {
        acid : NucleicAcid,
        axial : connectors::axial::AxialConnector,
        acid_con : connectors::acid::AcidConnector,
        sugar_con : connectors::sugar::SugarConnector,
    }

    impl Nucleotide {
        fn new(acid : NucleicAcid) -> Self {
            Nucleotide { acid : acid, axial : connectors::axial::AxialConnector::default(), 
                                acid_con : connectors::acid::AcidConnector::default(), sugar_con : connectors::sugar::SugarConnector::default()}

        }
    }
    
    #[allow(unused)]
    pub fn create_nucleotide(world : &mut World, acid : char) -> Entity {
        let _acid = match acid.to_uppercase().collect::<Vec<_>>()[0] {
            'C' => { NucleicAcid::C },
            'T' => { NucleicAcid::T },
            'G' => { NucleicAcid::G },
            _   => { NucleicAcid::A } 
        };

        let nuc = Nucleotide::new(_acid);
    
        world.spawn()
             .insert_bundle(nuc)
             .id()
    }

    impl From<&NucleicAcid> for char {
        fn from(acid : &NucleicAcid) -> Self {
            let out = match acid {
                NucleicAcid::A => 'A',
                NucleicAcid::C => 'C',
                NucleicAcid::T => 'T',
                NucleicAcid::G => 'G'
            };
    
            out
        }
    }
    
    #[derive(Clone,PartialEq,Default)]
    pub struct DNAStrand {
        pub nucleotides : Vec<Entity>,
    }

    #[allow(unused)]
    pub fn create_strand(world : &mut World, nucleotides: &str) -> Entity{
        let mut dna = DNAStrand::default();
        if nucleotides.len() > 0 {
        // I don't want to manipulate the input string, so clone it
        let cpy_ : &str = nucleotides.clone();
        let first_char = cpy_.chars().nth(0);
        let cpy_ : &str = &cpy_[1..];
        println!("Got char {}", first_char.unwrap());
        let mut prev_entity = create_nucleotide(world, first_char.unwrap());
        push_nucleotide(&mut dna, prev_entity);
    
        for c in cpy_.chars() {
            let next_entity = create_nucleotide(world, c);
            connectors::axial::connect(world, prev_entity, next_entity).ok();
            push_nucleotide(&mut dna, next_entity);
            prev_entity = next_entity;
        }
        }
        world.spawn().insert(dna).id()
    }

    
    #[derive(Clone,PartialEq)]
    pub struct DoubleDNAStrand {
        _3prime : Entity,
        _5prime : Entity,
    }

    pub (crate) fn push_nucleotide(dna : &mut DNAStrand, nucleotide : Entity) {
        dna.nucleotides.push(nucleotide);
    }
    
    #[allow(unused)]
    pub (crate) fn insert_nucleotide(world : &mut World, dna_entity : Entity, nucleotide : Entity, index : usize) -> Result<i32,&str> {
        world.get_entity(nucleotide).unwrap().get::<connectors::axial::AxialConnector>().ok_or("Not a valid nucleotide")?;

        let mut dna = world.get_entity_mut(dna_entity).unwrap().get_mut::<DNAStrand>().ok_or("Not a valid DNAStrand")?;

        let prev_entity = get_nucleotide(&dna, index-1);
        let next_entity = get_nucleotide(&dna, index);
        dna.nucleotides.insert(index, nucleotide);
        connectors::axial::connect(world, prev_entity, nucleotide).ok();
        connectors::axial::connect(world, nucleotide, next_entity).ok();

        Ok(0)
    }
    
    pub (crate) fn get_nucleotide(dna : &DNAStrand, index : usize) -> Entity {
        dna.nucleotides[index]
    }
    
    #[allow(unused)]
    pub (crate) fn into_string(world : &World, dna : Entity) -> Result<String,&str> {
        let dna = world.get_entity(dna).unwrap().get::<DNAStrand>().ok_or("Not a valid DNAStrand")?;
        let mut output = String::default();
        for n in dna.nucleotides.iter() {
            let nuc = world.get_entity(*n).unwrap().get::<NucleicAcid>().ok_or("Came across an element that is not a DNA nucleotide")?;
            output.push(nuc.into());
        }
        Ok(output)
    }
}

pub mod rna {
    use super::{Entity,World,Bundle};
    use crate::connectors;
    #[derive(Clone,Copy,PartialEq)]
    #[allow(dead_code)]
    pub enum NucleicAcid {
        A,
        C,
        U,
        G,
    }

    #[derive(Bundle)]
    pub struct Nucleotide {
        acid : NucleicAcid,
        axial : connectors::axial::AxialConnector,
        acid_con : connectors::acid::AcidConnector,
        sugar_con : connectors::sugar::SugarConnector,
    }

    impl Nucleotide {
        fn new(acid : NucleicAcid) -> Self {
            Nucleotide { acid : acid, axial : connectors::axial::AxialConnector::default(), 
                                acid_con : connectors::acid::AcidConnector::default(), sugar_con : connectors::sugar::SugarConnector::default()}

        }
    }
    
    impl From<&NucleicAcid> for char {
        fn from(acid : &NucleicAcid) -> Self {
            let out = match acid {
                NucleicAcid::A => 'A',
                NucleicAcid::C => 'C',
                NucleicAcid::U => 'U',
                NucleicAcid::G => 'G'
            };
    
            out
        }
    }
    
    #[derive(Clone,PartialEq)]
    pub struct RNAStrand {
        nucleotides : Vec<Entity>,
    }

    pub (crate) fn push_nucleotide(dna : &mut RNAStrand, nucleotide : Entity) {
        dna.nucleotides.push(nucleotide);
    }
    
    pub (crate) fn insert_nucleotide(dna : &mut RNAStrand, nucleotide : Entity, index : usize) {
        dna.nucleotides.insert(index, nucleotide);
    }
    
    pub (crate) fn get_nucleotide(dna : &RNAStrand, index : usize) -> Entity {
        dna.nucleotides[index]
    }
    
    pub (crate) fn into_string(world : &World, rna : Entity) -> String {
        let rna = world.get_entity(rna).unwrap().get::<RNAStrand>().unwrap();
        let mut output = String::default();
        for n in rna.nucleotides.iter() {
            let nuc = world.get_entity(*n).unwrap().get::<NucleicAcid>().unwrap();
            output.push(nuc.into());
        }
        output
    }
}