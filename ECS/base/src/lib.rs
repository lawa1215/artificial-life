mod genetic_components;
mod connectors;
mod protein;

#[cfg(test)]
mod tests {
    use bevy::ecs::world::World;
    use super::*;
    #[test]
    fn string_dna() {
        let mut world = World::default();
        let _str = "ACTG";
        let dna = genetic_components::dna::create_strand(&mut world, _str);
        let _out = genetic_components::dna::into_string(&world, dna);
        assert_eq!(_str,_out.unwrap());
    }

    #[test]
    fn insert_into_dna() {
        let mut world = World::default();
        let _str = "ACTG";
        let dna = genetic_components::dna::create_strand(&mut world, _str);
        let nucleotide = genetic_components::dna::create_nucleotide(&mut world, 'C');
        genetic_components::dna::insert_nucleotide(&mut world, dna, nucleotide, 1).ok();
        let _out = genetic_components::dna::into_string(&world, dna);
        assert_eq!("ACCTG",_out.unwrap());
    }

    #[test]
    fn validate_dna_string() {
        let mut world = World::default();
        let _str = "ACTG";
        let dna = genetic_components::dna::create_strand(&mut world, _str);
        // To prevent an immutable borrow against world from causing borrow-checker problems, encapsulate the entire checking logic
        // into its own scope.
        {
            let dna_ = world.get_entity(dna).unwrap().get::<genetic_components::dna::DNAStrand>().unwrap();
            for idx in 1..(_str.len() - 1) {
                let prev_ = genetic_components::dna::get_nucleotide(&dna_, idx-1);
                let cur_  = genetic_components::dna::get_nucleotide(&dna_, idx);
                let next_ = genetic_components::dna::get_nucleotide(&dna_, idx+1);

                assert_eq!(prev_,connectors::axial::previous(&world, cur_).unwrap());
                assert_eq!(next_,connectors::axial::next(&world, cur_).unwrap());
            }
        }
        let nucleotide = genetic_components::dna::create_nucleotide(&mut world, 'C');
        genetic_components::dna::insert_nucleotide(&mut world, dna, nucleotide, 1).ok();
        let dna_ = world.get_entity(dna).unwrap().get::<genetic_components::dna::DNAStrand>().unwrap();
        for idx in 1..(_str.len() - 1) {
            let prev_ = genetic_components::dna::get_nucleotide(&dna_, idx-1);
            let cur_  = genetic_components::dna::get_nucleotide(&dna_, idx);
            let next_ = genetic_components::dna::get_nucleotide(&dna_, idx+1);

            assert_eq!(prev_,connectors::axial::previous(&world, cur_).unwrap());
            assert_eq!(next_,connectors::axial::next(&world, cur_).unwrap());
        }
    }

    /*#[test]
    fn incomplete_dna() {
        let mut world = World::default();
        let dna = genetic_components::dna::create_strand(&mut world, "");
        assert_eq!(dna, None);
    }*/

    use connectors::Connectable;
    /*#[test]
    fn polymerase_reading() {
        let mut world = World::default();
        let dna = genetic_components::dna::create_strand(&mut world, "TATTAACTGCTATTA");
        let polymerase = protein::create_polymerase(&mut world, "TATTA");
        let target = protein::polymerase_target(&world, polymerase).unwrap();
        let dna_string = genetic_components::dna::into_string(&world, dna).ok().unwrap();
        let idx = dna_string.find(target).unwrap();
        let nucleotides = &(world.get_entity(dna).unwrap().get::<genetic_components::dna::DNAStrand>().unwrap().nucleotides.clone())[idx + target.len()..];

        let mut mrna = "";
        for n in nucleotides {
            {
                {
                let mut sugar_con = world.get_entity_mut(*n).unwrap().get_mut::<connectors::sugar::SugarConnector>().unwrap();
                sugar_con.connect(polymerase);
                }

                let mut pol_con = world.get_entity_mut(polymerase).unwrap().get_mut::<connectors::sugar::SugarConnector>().unwrap();
                pol_con.connect(*n);
            }
            {
                {
                let mut acid_con = world.get_entity_mut(*n).unwrap().get_mut::<connectors::acid::AcidConnector>().unwrap();
                acid_con.connect(polymerase);
                }

                let mut pol_con = world.get_entity_mut(polymerase).unwrap().get_mut::<connectors::acid::AcidConnector>().unwrap();
                pol_con.connect(*n);
            }
            {

            }
        }
        assert_eq!(idx,0);
    }*/
}
