use bevy::ecs::entity::Entity;
use bevy::ecs::world::{World};

#[allow(dead_code)]
pub mod axial {
    use super::*;
    #[derive(Clone,Copy,Default,PartialEq)]
    pub struct AxialConnector {
        next     : Option<Entity>,
        previous : Option<Entity>,
    }
    
    pub fn create_component(next : Option<Entity>, previous : Option<Entity>) -> AxialConnector {
        AxialConnector{next : next, previous : previous}
    }
    
    pub fn next(world : &World, this_ : Entity) -> Option<Entity> {
        let conn = world.get_entity(this_).unwrap().get::<AxialConnector>()?;
        let next = conn.next;
    
        next
    }
    
    pub fn previous(world : &World, this_ : Entity) -> Option<Entity> {
        let conn = world.get_entity(this_).unwrap().get::<AxialConnector>()?;
        let prev = conn.previous;
    
        prev
    }
    
    pub fn connect(world : &mut World, prev : Entity, next : Entity) -> Result<i32,&str> {
        world.get::<AxialConnector>(prev).ok_or("Entity input for 'prev' does not contain an AxialConnector")?;
        world.get::<AxialConnector>(next).ok_or("Entity input for 'next' does not contain an AxialConnector")?;
    
        let mut prev_axial = world.get_mut::<AxialConnector>(prev).unwrap();
        prev_axial.next = Some(next);
        let mut next_axial = world.get_mut::<AxialConnector>(next).unwrap();
        next_axial.previous = Some(prev);
        Ok(0)
    }
}

pub trait Connectable {
    fn is_connected(&self) -> bool;
    fn connected_entity(&self) -> Option<Entity>;
    fn connect(&mut self,entity : Entity);
}

#[allow(dead_code)]
pub (crate) fn has_connection<T : Connectable + Send + Sync + 'static>(world : &World, entity : Entity) -> bool {
    match world.get_entity(entity).unwrap().get::<T>() {
        Some(e) => {
            e.is_connected()
        },
        _ => { false }
    }
}

#[allow(dead_code)]
pub (crate) fn get_connected<T : Connectable + Send + Sync + 'static>(world : &World, entity : Entity) -> Option<Entity> {
    match world.get_entity(entity).unwrap().get::<T>() {
        Some(e) => {
            e.connected_entity()
            },
        _ => { None }
    }
}

pub mod acid {
    use super::{Entity,Connectable};
    #[derive(Clone,Copy,Default,PartialEq)]
    pub struct AcidConnector {
        connected : Option<Entity>,
    }

    impl Connectable for AcidConnector {
        fn is_connected(&self) -> bool {
            match self.connected {
                Some(_) => true,
                _       => false
            }
        }

        fn connected_entity(&self) -> Option<Entity> {
            self.connected
        }

        fn connect(&mut self, entity : Entity) {
            self.connected = Some(entity);
        }
    }

    #[allow(dead_code)]
    pub fn create(connection : Option<Entity>) -> AcidConnector {
        AcidConnector{connected : connection}
    }
}
pub mod sugar {
    use super::{Entity,Connectable};
    #[derive(Clone,Copy,Default,PartialEq)]
    pub struct SugarConnector {
        connected : Option<Entity>,
    }

    impl Connectable for SugarConnector {
        fn is_connected(&self) -> bool {
            match self.connected {
                Some(_) => true,
                _       => false
            }
        }

        fn connected_entity(&self) -> Option<Entity> {
            self.connected
        }

        fn connect(&mut self, entity : Entity) {
            self.connected = Some(entity);
        }
    }

    #[allow(dead_code)]
    pub fn create(connection : Option<Entity>) -> SugarConnector {
        SugarConnector{connected : connection}
    }
}

#[cfg(test)]
mod tests {
    use bevy::ecs::world::World;
    use super::*;
    #[test]
    fn sugar_connector() {
        let mut world = World::default();
        let e1 = world.spawn().id();
        let e2 = world.spawn().id();
        let mut entry1 = world.get_entity_mut(e1).unwrap();
        entry1.insert(sugar::create(Some(e2)));
        let mut entry2 = world.get_entity_mut(e2).unwrap();
        entry2.insert(sugar::create(Some(e1)));
        {
            assert_eq!(Some(e2), get_connected::<sugar::SugarConnector>(&world,e1));
        }
        assert_eq!(Some(e1), get_connected::<sugar::SugarConnector>(&world,e2));
    }
    #[test]
    fn acid_connector() {
        let mut world = World::default();
        let e1 = world.spawn().id();
        let e2 = world.spawn().id();
        let mut entry1 = world.get_entity_mut(e1).unwrap();
        entry1.insert(acid::create(Some(e2)));
        let mut entry2 = world.get_entity_mut(e2).unwrap();
        entry2.insert(acid::create(Some(e1)));
        {
            assert_eq!(Some(e2), get_connected::<acid::AcidConnector>(&world,e1));
        }
        assert_eq!(Some(e1), get_connected::<acid::AcidConnector>(&world,e2));
    }
    #[test]
    fn axial_connector() {
        let mut world = World::default();
        let e1 = world.spawn().id();
        let e2 = world.spawn().id();
        let mut entry1 = world.get_entity_mut(e1).unwrap();
        entry1.insert(axial::create_component(None, Some(e2)));
        let mut entry2 = world.get_entity_mut(e2).unwrap();
        entry2.insert(axial::create_component(Some(e1), None));
        {
            assert_eq!(Some(e2), axial::previous(&world, e1));
        }
        assert_eq!(Some(e1), axial::next(&world, e2));
    }
}