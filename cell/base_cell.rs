use crate::genetics_base::dna::{DnaStrand, DoubleDnaStrand};//, LockedNucleotide};
use crate::genetics_base::genetics_traits::*;//ProteinTrait;

use std::sync::{Arc,RwLock};
use std::collections::HashMap;

pub struct SphericalCoordinates{
    altitude: f32,
    azimuth : f32,
    radius  : f32,
}

pub struct Protein
{
    protein: Arc<dyn ProteinTrait>,
    position: RwLock<SphericalCoordinates>,
    cell: Arc<Cell>,
}

pub struct Chromatin
{
    dna: DoubleDnaStrand,
    histones: Vec<Arc<Histone>>,
}

pub struct DnaString
{
    _3prime: String,
    _5prime: String,
}

impl From<&DoubleDnaStrand> for DnaString
{
    fn from(_ddna : &DoubleDnaStrand) -> DnaString
    {
        DnaString{
            _3prime : String::from(_ddna._3prime().unwrap()),
            _5prime : String::from(_ddna._5prime().unwrap()),
        }
    }
}

pub struct PolymeraseI;

impl Polymerase for PolymeraseI {}
impl ProteinTrait for PolymeraseI 
{
    fn exec(&self, _cell: Arc<Cell>)
    {

    }
}
impl Attachable for PolymeraseI {}
impl Extension for PolymeraseI {}
impl Connectable for PolymeraseI {}

pub struct PolymeraseII;

impl Polymerase for PolymeraseII {}
impl ProteinTrait for PolymeraseII 
{
    fn exec(&self, _cell: Arc<Cell>)
    {

    }
}
impl Attachable for PolymeraseII {}
impl Extension for PolymeraseII {}
impl Connectable for PolymeraseII {}

pub struct Cell
{
    chromosomes : RwLock<Vec<RwLock<Arc<Chromatin>>>>,
    stringified_dna : RwLock<Vec<Arc<DnaString>>>,
    proteins : RwLock<Vec<Arc<Protein>>>,
    receptors : RwLock<Vec<Arc<Protein>>>,
    transports : RwLock<Vec<Arc<Protein>>>,
    targeting : RwLock<Vec<Arc<Protein>>>,
    polymerase_i : RwLock<Vec<Arc<PolymeraseI>>>,
    polymerase_ii : RwLock<Vec<Arc<PolymeraseII>>>,
    position : RwLock<Arc<SphericalCoordinates>>,
}

impl Cell
{
    pub fn new( genetics: Vec<RwLock<Arc<Chromatin>>>, 
    pol_i: PolymeraseI, pol_ii: PolymeraseII, tgt_units: Vec<Protein>, 
    pos : SphericalCoordinates ) -> Cell
    {
        // Construct DNA
        let copied_genetics : Vec<DnaString> = genetics.into_iter().map(|g| DnaString::from(&g.read().unwrap().dna)).collect();

        // Create Histones
        // Attach Histones to proper location, and wind up X number nucleotides
        // Construct Chromatin
        Cell{chromosomes: RwLock::new(Default::default()),
        stringified_dna: RwLock::new( copied_genetics.into_iter().map(|g| Arc::new(g)).collect() ),
        proteins: RwLock::new(Default::default()),
        receptors: RwLock::new(Default::default()),
        transports: RwLock::new(Default::default()),
        targeting: RwLock::new( tgt_units.into_iter().map(|t| Arc::new(t)).collect() ),
        polymerase_i: RwLock::new( vec![Arc::new(pol_i)] ),
        polymerase_ii: RwLock::new( vec![Arc::new(pol_ii)] ),
        position : RwLock::new(Arc::new(pos)),
        }
    }
}
