use std::sync::{Weak,RwLock};
use std::any::Any;

pub struct Link
{
    connected: RwLock<Option<Weak<dyn Attachable>>>,
}

impl Link
{
    pub fn new() -> Self
    {
        Self{
            connected: RwLock::new(None),
        }
    }
    
    pub fn disconnect(&self)
    {
        match self.connected.write()
        {
            Ok(mut l) => *l = None,
            _ => {}
        }
    }

    pub fn connect(&self, c: Weak<dyn Attachable>)
    {
        match self.connected.write()
        {
            Ok(mut l) => *l = Some(c.clone()),
            _ => {}
        }
    }

    pub fn get_connected(&self) -> Result<Weak<dyn Attachable>,i8>
    {
        let r = self.connected.read().unwrap();
        match &*r
        {
            Some(c) => 
            { 
                Ok(c.clone())
            }
            _ => Err(0)
        }
    }
}

pub trait Attachable
{
    fn detach(&self) {
        match self.attached()
        {
            Ok(a) => a.disconnect(),
            _ => {}
        }
    }
    fn attach(&self, _c: Weak<dyn Attachable>) {}
    fn attached(&self) -> Result<&Link,i8> { Err(-1) }

    fn as_any(&self) -> &dyn Any;
}
pub trait Extension
{
    fn dissassemble(&self) {
        match self.extension()
        {
            Ok(a) => a.disconnect(),
            
            Err(_) => {}
        }
    }
    fn extend(&self, _c: Weak<dyn Attachable>) {}
    fn extension(&self) -> Result<&Link,i8> { Err(-1) }

    fn as_any(&self) -> &dyn Any;
}