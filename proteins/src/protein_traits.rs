use generic_traits::Attachable;

// This is potentially all wrong.  Use the ECS architecture to handle various protein types.
// Maybe it would be best to return a builder object, that can register entities and components into
// the world in which the protein originated from.  This should allow for any environment to originate
// proteins, and those proteins can have whatever components associated with them at construction-time.

// Proteins can inhabit multiple environments.  Could be in a cell.
// Could be in an extra-cellular area (like in plasma, or between neurons, etc.).
// So... how do I do that?
pub trait ProteinTrait : Attachable
{
    fn exec(&self);
}

struct Protein
{
    detail : Box<dyn ProteinTrait>,
}

pub struct Histone
{
    captured_nucleotides: RwLock<Vec<Weak<Nucleotide>>>,
}

impl Attachable for Histone {}
impl ProteinTrait for Histone 
{
    fn exec(&self)
    {

    }
}
