pub mod nucleotide;
pub mod dna;

#[cfg(test)]
mod tests {
    use generic_traits::Attachable;
    use crate::dna::{DnaStrand, DoubleDnaStrand};
    use std::sync::{Arc, Weak};
    use crate::nucleotide::{NucleotideEnum, Nucleotide};

    const NEW_LOCKEDNUCLEOTIDE : fn(n : NucleotideEnum)->Arc<Nucleotide> =
        |x| Arc::new(Nucleotide::new(x));
    
    #[test]
    fn from_string_to_dna() {
        let s = "ACTG".to_string();
        let dna : DnaStrand = (&s).into();
        let nucs : &Vec<Arc<Nucleotide>> = &*dna.nucleotides().read().unwrap();
        let output : String = nucs.into_iter().map(|n| char::from(&n.get_elem())).collect();
        assert_eq!(output,s);
    }

    #[test]
    fn from_dna_to_string() {
        let mut dna : DnaStrand = DnaStrand::new();
        dna.add_nucleotide(NucleotideEnum::A);
        dna.add_nucleotide(NucleotideEnum::C);
        dna.add_nucleotide(NucleotideEnum::T);
        dna.add_nucleotide(NucleotideEnum::G);

        let s : String = dna.into();

        assert_eq!("ACTG".to_string(),s);
    }

    #[test]
    fn add_to_single_dna_strand() {
        let g = NEW_LOCKEDNUCLEOTIDE(NucleotideEnum::A);
        let n = NEW_LOCKEDNUCLEOTIDE(NucleotideEnum::C);
        let u = NEW_LOCKEDNUCLEOTIDE(NucleotideEnum::G);

        let mut v = DnaStrand::new();
        v.push_back(g);
        v.push_back(n);
        v.push_back(u);
        v.push_back(NEW_LOCKEDNUCLEOTIDE(NucleotideEnum::T));

        for n in v.nucleotides().read().into_iter().enumerate()
        {
            if n.0 == 0
            {
                continue;
            }
            assert_eq!(
                Arc::ptr_eq(v.nucleotides()
                             .read().unwrap()
                             .get(n.0).unwrap(),
                            v.nucleotides()
                             .read().unwrap()
                             .get(n.0-1).unwrap()),
                            true);
        }
    }
    #[test]
    fn add_nucleotide_to_double_dna_strand() {
        let mut v = DoubleDnaStrand::new();
        v.add_nucleotide(NucleotideEnum::A);
        v.add_nucleotide(NucleotideEnum::C);
        v.add_nucleotide(NucleotideEnum::T);
        v.add_nucleotide(NucleotideEnum::G);

        // Testing ability to detach and reattach the same nucleotide
        let _3prime = v._3prime()
                       .nucleotides()
                       .read().unwrap();
        let _5prime = v._5prime()
                       .nucleotides()
                       .read().unwrap();
        _3prime.get(0).unwrap().detach();
        _3prime.get(1).unwrap().detach();
        _3prime.get(2).unwrap().detach();
        _3prime.get(3).unwrap().detach();
        _3prime.get(0).unwrap().attach(
            Arc::downgrade(_5prime.get(0).unwrap()) 
              as Weak<dyn Attachable>);
        _3prime.get(1).unwrap().attach(
            Arc::downgrade(_5prime.get(1).unwrap()) 
              as Weak<dyn Attachable>);
        _3prime.get(2).unwrap().attach(
            Arc::downgrade(_5prime.get(2).unwrap()) 
              as Weak<dyn Attachable>);
        _3prime.get(3).unwrap().attach(
            Arc::downgrade(_5prime.get(3).unwrap()) 
              as Weak<dyn Attachable>);
   
        for n in _3prime.iter().enumerate()
        {
            if n.0 == 0
            {
                continue;
            }
            assert_eq!(Arc::downgrade(&_3prime.get(n.0).unwrap()).ptr_eq(
                        &_3prime.get(n.0-1).unwrap().next().unwrap()),true);
        }
    }
    #[test]
    fn add_nucleotide_to_big_double_dna_strand() {
        let mut v = DoubleDnaStrand::new();
        const SIZE : usize = 1_000_000;

        (0..(SIZE-1)).for_each(|_| {
          v.add_nucleotide(NucleotideEnum::A);
        });
        
        ((SIZE-10)..(SIZE-1)).for_each(|i| { 
          v._3prime().nucleotides().read().unwrap().get(i).unwrap().detach();
          v._3prime().nucleotides().read().unwrap().get(i).unwrap().attach(Arc::downgrade(v._5prime().nucleotides().read().unwrap().get(i).unwrap()) as Weak<dyn Attachable>);
        });

        (1..10).for_each(|i| {
          assert_eq!(Arc::downgrade(&v._3prime().nucleotides().read().unwrap().get(i+1).unwrap()).ptr_eq(
                      &v._3prime().nucleotides().read().unwrap().get(i).unwrap().next().unwrap()),true);
          assert_eq!(Arc::downgrade(&v._5prime().nucleotides().read().unwrap().get(i+1).unwrap()).ptr_eq(
                      &v._5prime().nucleotides().read().unwrap().get(i).unwrap().next().unwrap()),true);

          assert_eq!((Arc::as_ptr(&v._3prime().nucleotides().read().unwrap().get(i).unwrap()) as *const u64),
                      (Arc::as_ptr(&v._5prime().nucleotides().read().unwrap().get(i).unwrap().attached().unwrap().get_connected().unwrap().upgrade().unwrap()) as *const u64));
          assert_eq!((Arc::as_ptr(&v._5prime().nucleotides().read().unwrap().get(i).unwrap()) as *const u64),
                      (Arc::as_ptr(&v._3prime().nucleotides().read().unwrap().get(i).unwrap().attached().unwrap().get_connected().unwrap().upgrade().unwrap()) as *const u64));
        });
    }
}
