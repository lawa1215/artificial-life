use std::sync::{Arc,Weak,RwLock};
use crate::nucleotide::{Nucleotide, NucleotideEnum, connect_nucleotides};
use generic_traits::{Attachable};

pub struct DnaStrand
{
  nucleotides: RwLock<Vec<Arc<Nucleotide>>>,
}

impl From<&DnaStrand> for String
{
  fn from(_dna: &DnaStrand) -> String
  {
    let nuc_v = &*_dna.nucleotides().read().unwrap();
    let str_dna = nuc_v.into_iter()
                       .map(
                           |nuc| 
                            char::from(&(nuc.get_elem()))
                       )
                       .collect();
    str_dna
  }
}

impl From<DnaStrand> for String
{
  fn from(_dna: DnaStrand) -> String
  {
    let nuc_v = &*_dna.nucleotides().read().unwrap();
    let str_dna = nuc_v.into_iter()
                       .map(
                           |nuc| 
                            char::from(&(nuc.get_elem()))
                       )
                       .collect();
    str_dna
  }
}

impl From<&String> for DnaStrand
{
  fn from(_from: &String) -> DnaStrand
  {
    let mut dna = DnaStrand::new();
    for c in _from.chars()
    {
      let nuc = Arc::new(Nucleotide::from(c));
      dna.push_back(nuc);
    }
    dna
  }
}

impl From<String> for DnaStrand
{
  fn from(_from: String) -> DnaStrand
  {
    let mut dna = DnaStrand::new();
    for c in _from.chars()
    {
      let nuc = Arc::new(Nucleotide::from(c));
      dna.push_back(nuc);
    }
    dna
  }
}

impl DnaStrand
{
  pub fn new() -> DnaStrand
  {
    DnaStrand {
        nucleotides: RwLock::new(Vec::new()),
    }
  }

  pub fn nucleotides(&self) -> &RwLock<Vec<Arc<Nucleotide>>>
  {
    &self.nucleotides
  }

  pub fn replace_nucleotide(&mut self, idx: usize, n: NucleotideEnum)
  {
    self.nucleotides.write().unwrap()[idx] = Arc::new(Nucleotide::new(n));
  }

  pub fn insert_nucleotide(&mut self, idx: usize, n: NucleotideEnum)
  {
    if idx < self.nucleotides.read().unwrap().len() {
      {
        self.nucleotides.write().unwrap().insert(idx, Arc::new(Nucleotide::new(n)));
      }
      connect_nucleotides(&self.nucleotides.read().unwrap()[idx],
      &self.nucleotides.read().unwrap()[idx+1]);
    }
  }

  pub fn add_nucleotide(&mut self, n: NucleotideEnum)
  {
    {
      self.nucleotides.write().unwrap().push(Arc::new(Nucleotide::new(n)));
    }
    
    let len = self.nucleotides.read().unwrap().len();
    
    if len > 1
    {
      connect_nucleotides(&self.nucleotides.read().unwrap()[len-1], 
                          &self.nucleotides.read().unwrap()[len-2]);
    }
  }

  pub fn push_back(&mut self, n: Arc<Nucleotide>)
  {
    {
      self.nucleotides.write().unwrap().push(n);
    }
    let len = self.nucleotides.read().unwrap().len();
    if len > 1
    {
      connect_nucleotides(&self.nucleotides.read().unwrap()[len-1], 
                          &self.nucleotides.read().unwrap()[len-2]);
    }
  }
}

pub struct DoubleDnaStrand
{
  _3prime: DnaStrand,
  _5prime: DnaStrand,
}

impl DoubleDnaStrand
{
  pub fn new() -> DoubleDnaStrand
  {
    DoubleDnaStrand {
      _3prime: DnaStrand::new(),
      _5prime: DnaStrand::new(),
    }
  }

  fn backfill(_nuc : &Nucleotide, _strand : &mut DnaStrand)
  {
    match _nuc.prev()
    {
      Some(_n) => 
      {
        match _n.upgrade()
        {
          Some(__n) =>
            {
              _strand.insert_nucleotide(0, __n.get_elem());
              DoubleDnaStrand::backfill(&*__n,_strand);
            },
          None => {}
        }
      },
      None =>
      {
      }
    }
  }

  fn forwardfill(_nuc : &Nucleotide, _strand : &mut DnaStrand)
  {
    match _nuc.next()
    {
      Some(_n) => 
      {
        match _n.upgrade()
        {
          Some(__n) =>
            {
              _strand.add_nucleotide(__n.get_elem());
              DoubleDnaStrand::forwardfill(&*__n,_strand);
            },
          None => {}
        }
      },
      None =>
      {
      }
    }
  }

  pub fn copy(_ddna: &DoubleDnaStrand) -> DoubleDnaStrand
  {
    let mut new_dna = 
        DoubleDnaStrand{_3prime: DnaStrand::new(), _5prime: DnaStrand::new()};
    for n in &*_ddna._3prime.nucleotides.read().unwrap()
    {
      let new_3p = Nucleotide::new(n.get_elem());
      if let Ok(conn) = n.attached() {
        if let Ok(conn) = conn.get_connected() {
          match conn.upgrade()
          {
            Some(_conn) =>
              {
                match _conn.as_any().downcast_ref::<Nucleotide>()
                {
                  Some(n) =>
                    {
                      let new_5p = Arc::new(Nucleotide::new(n.get_elem()));
                      new_3p.attach(Arc::downgrade(&new_5p) 
                                    as Weak<dyn Attachable>);
                      if new_dna._3prime.nucleotides.read().unwrap().len() == 0
                      {
                        DoubleDnaStrand::backfill(n, &mut new_dna._5prime);
                      }
                      new_dna._5prime.push_back(new_5p);
                        new_dna._3prime.push_back(Arc::new(new_3p));
                      },
                    None => {}
                  }
                }
              _ => {}
            }
          }
        } else {}
    }
    let last = _ddna._3prime.nucleotides.read();

    match last.unwrap().last().unwrap().attached()
    {
      Ok(conn) =>
      {
        match conn.get_connected()
        {
          Ok(conn) => 
          {
            match conn.upgrade()
            {
              Some(_conn) => {
                match _conn.as_any().downcast_ref::<Nucleotide>()
                {
                  Some(n) =>
                    {
                      DoubleDnaStrand::forwardfill(n, &mut new_dna._5prime);
                    },
                  None => {}
                }
              },
              None => {}
            }
          },
          Err(_) => {}
        }
      },
      Err(_) => {}
    }
    new_dna
  }

  pub fn _3prime(&self) -> &DnaStrand
  {
    &self._3prime
  }

  pub fn _5prime(&self) -> &DnaStrand
  {
    &self._5prime
  }

  pub fn add_nucleotide(&mut self, n: NucleotideEnum)
  {
    let nuc = Arc::new(Nucleotide::new(n));
    let _nuc = Arc::new(nuc.get_matched());
    nuc.attach(Arc::downgrade(&_nuc) as Weak<dyn Attachable>);
    _nuc.attach(Arc::downgrade(&nuc) as Weak<dyn Attachable>);
    self._3prime.push_back(nuc);
    self._5prime.push_back(_nuc);
  }

  pub fn push_back(&mut self, n: Arc<Nucleotide>)
  {
    let nuc = Arc::new(n.get_matched());
    nuc.attach(Arc::downgrade(&n) as Weak<dyn Attachable>);
    n.attach(Arc::downgrade(&nuc) as Weak<dyn Attachable>);
    self._3prime.push_back(n);
    self._5prime.push_back(nuc);
  }
}
