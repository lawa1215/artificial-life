use generic_traits::{Link, Attachable, Extension};
use std::any::Any;
use std::sync::{Arc,Weak,RwLock};

pub fn connect_nucleotides(n: &Arc< Nucleotide >, p: &Arc< Nucleotide >)
{
  *p.next.write().unwrap() = Some(Arc::downgrade(&n));
  *n.prev.write().unwrap() = Some(Arc::downgrade(&p));
}

#[derive(Debug,Clone,Copy,PartialEq)]
pub enum NucleotideEnum
{
  A,
  C,
  T,
  G,
}

type WeakNucleotidePtr = Option< Weak< Nucleotide > >;
pub struct Nucleotide
{
  elem: NucleotideEnum,
  next: RwLock< WeakNucleotidePtr >,
  prev: RwLock< WeakNucleotidePtr >,
  attached: Link,
  extension: Link,
}

impl std::fmt::Debug for Nucleotide
{
  fn fmt(&self, _formatter: &mut std::fmt::Formatter) 
      -> std::result::Result<(), std::fmt::Error>
  {
    unimplemented!()
  }
}

impl From<&NucleotideEnum> for char
{
  fn from(_from:&NucleotideEnum) -> char
  {
    match _from
    {
      NucleotideEnum::A => 'A',
      NucleotideEnum::C => 'C',
      NucleotideEnum::T => 'T',
      NucleotideEnum::G => 'G',
    }
  }
}

impl From<char> for NucleotideEnum
{
  fn from(_from:char) -> NucleotideEnum
  {
    match _from
    {
      'a' => NucleotideEnum::A,
      'A' => NucleotideEnum::A,
      'c' => NucleotideEnum::C,
      'C' => NucleotideEnum::C,
      't' => NucleotideEnum::T,
      'T' => NucleotideEnum::T,
      'g' => NucleotideEnum::G,
      'G' => NucleotideEnum::G,
      _ => NucleotideEnum::A,
    }
  }
}

impl From<char> for Nucleotide
{
  fn from(_from:char) -> Nucleotide
  {
    match _from
    {
      'a' => Nucleotide::new(NucleotideEnum::A),
      'A' => Nucleotide::new(NucleotideEnum::A),
      'c' => Nucleotide::new(NucleotideEnum::C),
      'C' => Nucleotide::new(NucleotideEnum::C),
      't' => Nucleotide::new(NucleotideEnum::T),
      'T' => Nucleotide::new(NucleotideEnum::T),
      'g' => Nucleotide::new(NucleotideEnum::G),
      'G' => Nucleotide::new(NucleotideEnum::G),
      _ => Nucleotide::new(NucleotideEnum::A),
    }
  }
}

impl Nucleotide
{
  pub fn new(elem: NucleotideEnum) -> Nucleotide
  {
    Self{
      elem,
      next : RwLock::new(None),
      prev : RwLock::new(None),
      attached: Link::new(),
      extension: Link::new(),
    }
  }

  pub fn next(&self) -> WeakNucleotidePtr
  {
    match &*self.next.read().unwrap()
    {
      Some(n) => Some(n.clone()),
      None => None
    }
  }

  pub fn prev(&self) -> WeakNucleotidePtr
  {
    match &*self.prev.read().unwrap()
    {
      Some(n) => Some(n.clone()),
      None => None,
    }
  }

  pub fn get_matched(&self) -> Nucleotide
  {
    match self.elem
    {
      NucleotideEnum::A => Nucleotide::new(NucleotideEnum::T),
      NucleotideEnum::T => Nucleotide::new(NucleotideEnum::A),
      NucleotideEnum::C => Nucleotide::new(NucleotideEnum::G),
      NucleotideEnum::G => Nucleotide::new(NucleotideEnum::C),
    }
  }

  pub fn get_matched_elem(&self) -> NucleotideEnum
  {
    match self.elem
    {
      NucleotideEnum::A => NucleotideEnum::T,
      NucleotideEnum::T => NucleotideEnum::A,
      NucleotideEnum::C => NucleotideEnum::G,
      NucleotideEnum::G => NucleotideEnum::C,
    }
  }

  pub fn get_elem(&self) -> NucleotideEnum
  {
    self.elem
  }
}

impl Extension for Nucleotide
{
  fn extend(&self, c: Weak<dyn Attachable>)
  {
    self.extension.connect(c);
  }
  fn extension(&self) -> Result<&Link,i8>
  {
    Ok(&self.extension)
  }
  fn as_any(&self) -> &dyn Any
  {
    self
  }
}

impl Attachable for Nucleotide
{
  fn attach(&self, c: Weak<dyn Attachable>)
  {
    self.attached.connect(c);
  }
  fn attached(&self) -> Result<&Link,i8>
  {
    Ok(&self.attached)
  }
  fn as_any(&self) -> &dyn Any
  {
      self
  }
}

