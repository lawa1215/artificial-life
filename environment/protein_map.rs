use std::sync::{Arc, RwLock};
use std::collections::HashMap;
use crate::genetics_base::genetics_traits::{ProteinTrait, Connectable, Extension, Attachable};
use crate::cell::base_cell::Cell;

struct ProteinMap;

// Correlate amino acid strings to protein names (This is to make it easier to write configuration files dictating which amino acid chains go to which proteins)
thread_local!
{
    static PROTEIN_MAP: RwLock<Arc<HashMap<&'static str, &'static str>>> = RwLock::new(Default::default());
}

impl ProteinMap
{
    pub fn get() -> Arc<HashMap<&'static str, &'static str>>
    {
        PROTEIN_MAP.with(|m| m.read().unwrap().clone())
    }
    pub fn translate(amino_chain: &'static str) -> String
    {
        let mut ret = String::new();
        ret = "nonsense".to_string();
        // In the real code, a fuzzy match between amino_chain and the keys will be performed
        if ProteinMap::get().contains_key(amino_chain) 
        {
            return ProteinMap::get()[amino_chain].to_string();
        }
        ret
    }
    pub fn add_entry(_id : &'static str, _protein: &'static str)
    {
        let current_map = PROTEIN_MAP.with(|c| (*c.read().unwrap().clone()).clone());
        let mut new_map = current_map.clone();
        new_map.insert(_id, _protein);
        PROTEIN_MAP.with(|m| *m.write().unwrap() = Arc::new(new_map));
    }
}

struct NonsenseProtein;

impl Connectable for NonsenseProtein {}
impl Attachable for NonsenseProtein {}
impl Extension for NonsenseProtein {}

impl ProteinTrait for NonsenseProtein 
{
    fn exec(&self, _cell: Arc<Cell>)
    {

    }
}

fn make_nonsense() -> Arc<dyn ProteinTrait>
{
    Arc::new(NonsenseProtein)
}

type ProteinCreator = fn()->Arc<dyn ProteinTrait>;

thread_local!
{
    static CREATOR_MAP: RwLock<Arc<HashMap<&'static str, ProteinCreator>>> = RwLock::new(Default::default());
}

// This looks weird, but it works.  Basically, I need a ProteinCreatorMap implementation so that I can maintain the map of protein creators
pub struct ProteinCreatorMap;

impl ProteinCreatorMap
{
    pub fn get() -> Arc<HashMap<&'static str, ProteinCreator>>
    {
        CREATOR_MAP.with(|m| m.read().unwrap().clone())
    }
    pub fn add_entry(_id : &'static str, _creator : ProteinCreator)
    {
        let current_map = CREATOR_MAP.with(|c| (*c.read().unwrap().clone()).clone());
        let mut new_map = current_map.clone();
        new_map.insert(_id, _creator);
        CREATOR_MAP.with(|m| *m.write().unwrap() = Arc::new(new_map));
    }
}

#[cfg(test)]
mod tests
{
    struct NovelProtein;

    impl Connectable for NovelProtein {}
    impl Attachable for NovelProtein {}
    impl Extension for NovelProtein {}

    impl ProteinTrait for NovelProtein 
    {
        fn exec(&self, cell: Arc<Cell>)
        {

        }
    }

    fn make_novel() -> Arc<dyn ProteinTrait>
    {
        Arc::new(NovelProtein)
    }
    use super::*;
    #[test]
    fn add_to_creator_map()
    {
        ProteinCreatorMap::add_entry("nonsense", make_nonsense);
        ProteinCreatorMap::add_entry("novel", make_novel);
        assert_eq!(ProteinCreatorMap::get().keys().len(),2);
    }

    #[test]
    fn add_to_protein_map()
    {
        ProteinCreatorMap::add_entry("novel", make_novel);
        ProteinMap::add_entry("YLW", "novel");
        ProteinCreatorMap::get()[ProteinMap::translate("YLW").as_str()]();
    }

    #[test]
    #[should_panic]
    fn bad_attempt_to_get_protein()
    {
        ProteinCreatorMap::add_entry("novel", make_novel);
        ProteinMap::add_entry("YLW", "novel");
        ProteinCreatorMap::get()[ProteinMap::translate("YLU").as_str()]();
    }
}