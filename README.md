# README #

This project attempts to simulate life down to the genetics level.  The only major point of abstraction is the 
functionality of the proteins.  Reading of genetics is based on biology.  While not fully implemented, the intent
is to have a polymerase attach to a target site based on subunits.  When the polymerase attaches it reads the DNA
sequentially.  Each subunit defines the target and stop codons.

### How do I get set up? ###

This repo is like any other Rust project.  Use "cargo build" to build, and "cargo test" to run the tests.

### Contribution guidelines ###

The only guidance I have for this project is this: do not allow for transcription "shortcuts."  Meaning, all proteins
must be created from the full transcription-to-translation pipeline.  Then the resulting "mRNA" must bind to ribosomes.
Proteins can do anything.  They can bind to other proteins, they can traverse through cell barriers, and so on.  

### Who do I talk to? ###

Aaron Law is the owner, and will answer any questions, or respond to criticism regarding this repository.

Contact him at lawa@email.arizona.edu