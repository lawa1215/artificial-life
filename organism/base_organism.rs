use std::sync::{Arc};//, Mutex};
//use std::cell::RefCell;
//use crate::genetics_base::genetics_traits::ProteinTrait;
use crate::cell::base_cell::Cell;
//use std::collections::HashMap;

struct Organism
{
    //protein_map: RefCell<HashMap<String, fn()->Box<dyn ProteinTrait>>>, // registered protein creators associated with protein identifiers
    //amino_acid_sequences: Arc<HashMap<String,String>>, // correlate known amino acid sequences to protein identifiers
    cells: Vec<Arc<Cell>>,
}