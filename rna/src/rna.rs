use dna::nucleotide::{Nucleotide, NucleotideEnum};
use generic_traits::{Link, Attachable, Extension};
use std::any::Any;
use std::sync::{Weak, RwLock};

#[derive(Debug,Clone,Copy,PartialEq)]
pub enum RnaNucleotideEnum
{
    A,
    C,
    U,
    G,
}

type WeakRnaNucleotidePtr = Option< Weak< RnaNucleotide > >;

pub struct RnaNucleotide
{
    elem: RnaNucleotideEnum,
    pub (crate) next: RwLock<WeakRnaNucleotidePtr>,
    pub (crate) prev: RwLock<WeakRnaNucleotidePtr>,
    attached: Link,
    extension: Link,
}

impl std::fmt::Display for RnaNucleotide
{
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error>
    {
        match self.get_elem()
        {
            RnaNucleotideEnum::A => write!(formatter,"A"),
            RnaNucleotideEnum::C => write!(formatter,"C"),
            RnaNucleotideEnum::G => write!(formatter,"G"),
            RnaNucleotideEnum::U => write!(formatter,"U"),
        }
    }
}
impl std::fmt::Debug for RnaNucleotide
{
    fn fmt(&self, _formatter: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error>
    {
        unimplemented!()
    }
}

impl RnaNucleotide
{
    pub fn new(elem: RnaNucleotideEnum) -> RnaNucleotide
    {
        Self{
            elem,
            next : RwLock::new(None),
            prev : RwLock::new(None),
            attached: Link::new(),
            extension: Link::new(),
        }
    }

    pub fn next(&self) -> WeakRnaNucleotidePtr
    {
        match &*self.next.read().unwrap()
        {
            Some(n) => Some(n.clone()),
            None => None,
        }
    }

    pub fn prev(&self) -> WeakRnaNucleotidePtr
    {
        match &*self.prev.read().unwrap()
        {
            Some(n) => Some(n.clone()),
            None => None,
        }
    }

    pub fn get_matched(n: &Nucleotide) -> RnaNucleotide
    {
        match n.get_elem()
        {
            NucleotideEnum::A => RnaNucleotide::new(RnaNucleotideEnum::U),
            NucleotideEnum::T => RnaNucleotide::new(RnaNucleotideEnum::A),
            NucleotideEnum::C => RnaNucleotide::new(RnaNucleotideEnum::G),
            NucleotideEnum::G => RnaNucleotide::new(RnaNucleotideEnum::C),
        }
    }

    pub fn get_matched_elem(n: &Nucleotide) -> RnaNucleotideEnum
    {
        match n.get_elem()
        {
            NucleotideEnum::A => RnaNucleotideEnum::U,
            NucleotideEnum::T => RnaNucleotideEnum::A,
            NucleotideEnum::C => RnaNucleotideEnum::G,
            NucleotideEnum::G => RnaNucleotideEnum::C,
        }
    }

    pub fn get_elem(&self) -> RnaNucleotideEnum
    {
        self.elem
    }
}

impl Attachable for RnaNucleotide
{
    fn attach(&self, c: Weak<dyn Attachable>)
    {
        self.attached.connect(c);
    }
    fn attached(&self) -> Result<&Link,i8>
    {
        Ok(&self.attached)
    }
    fn as_any(&self) -> &dyn Any
    {
        self
    }
}

impl Extension for RnaNucleotide
{
    fn extend(&self, c: Weak<dyn Attachable>)
    {
        self.extension.connect(c);
    }

    fn extension(&self) -> Result<&Link,i8>
    {
        Ok(&self.extension)
    }
    fn as_any(&self) -> &dyn Any
    {
        self
    }
}