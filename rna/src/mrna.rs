use crate::rna::{RnaNucleotide,RnaNucleotideEnum};
use std::sync::Arc;

impl std::fmt::Display for mRNA
{
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error>
    {
        let nucs = &self.nucleotides;
        let mut out : String = "".to_string();
        nucs.into_iter().for_each(
            |n| {
                out.push_str(&*n.to_string());
            }
        );
        write!(formatter,"{}",out)
    }
}
#[allow(non_camel_case_types)]
pub struct mRNA
{
    nucleotides: Vec<Arc<RnaNucleotide>>,
}

impl mRNA
{
    pub fn new() -> mRNA
    {
        mRNA {
            nucleotides: Vec::new(),
        }
    }

    pub fn replace_nucleotide(&mut self, idx: usize, n: RnaNucleotideEnum)
    {
        if idx < self.nucleotides.len() {
            self.nucleotides[idx] = Arc::new(RnaNucleotide::new(n));
            if idx + 1 < self.nucleotides.len()
            {
                *self.nucleotides[idx].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx+1]));
                *self.nucleotides[idx+1].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx]));
            }
            else
            {
                *self.nucleotides[idx].next.write().unwrap() = None;
            }
            if idx > 0
            {
                *self.nucleotides[idx-1].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx]));
                *self.nucleotides[idx].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx-1]));
            }
            else
            {
                *self.nucleotides[idx].prev.write().unwrap() = None;
            }
        }
    }

    pub fn insert_nucleotide(&mut self, idx: usize, n: RnaNucleotideEnum)
    {
        if idx < self.nucleotides.len() {
            self.nucleotides.insert(idx, Arc::new(RnaNucleotide::new(n)));
            if idx + 1 < self.nucleotides.len()
            {
                *self.nucleotides[idx].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx+1]));
                *self.nucleotides[idx+1].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx]));
            }
            else
            {
                *self.nucleotides[idx].next.write().unwrap() = None;
            }
            if idx > 0
            {
                *self.nucleotides[idx-1].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx]));
                *self.nucleotides[idx].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx-1]));
            }
            else
            {
                *self.nucleotides[idx].prev.write().unwrap() = None;
            }
        }
    }

    pub fn insert_nucleotide_slice(&mut self, idx: usize, n: &[Arc<RnaNucleotide>])
    {
        if idx < self.nucleotides.len() {
            self.nucleotides.splice(idx..idx, n.iter().cloned());
            if idx + n.len() < self.nucleotides.len()
            {
                *self.nucleotides[idx + n.len() - 1].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx+n.len()]));
                *self.nucleotides[idx + n.len()].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx+n.len()-1]));
            }
            else
            {
                *self.nucleotides[idx + n.len() - 1].next.write().unwrap() = None;
            }
            if idx > 0
            {
                *self.nucleotides[idx-1].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx]));
                *self.nucleotides[idx].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[idx-1]));
            }
            else
            {
                *self.nucleotides[idx].prev.write().unwrap() = None;
            }
        }
    }

    pub fn rna_nucleotides(&self) -> Result<&Vec<Arc<RnaNucleotide>>,i8>
    {
        Ok(&self.nucleotides)
    }

    pub fn add_nucleotide(&mut self, n: RnaNucleotideEnum)
    {
        self.nucleotides.push(Arc::new(RnaNucleotide::new(n)));
        
        let len = self.nucleotides.len();
        if len > 1
        {
            *self.nucleotides[len-2].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[len-1]));
            *self.nucleotides[len-1].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[len-1]));
        }
    }

    pub fn push_back(&mut self, n: Arc<RnaNucleotide>)
    {
        self.nucleotides.push(n);

        let len = self.nucleotides.len();
        if len > 1
        {
            *self.nucleotides[len-2].next.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[len-1]));
            *self.nucleotides[len-1].prev.write().unwrap() = Some(Arc::downgrade(&self.nucleotides[len-1]));
        }
    }
}