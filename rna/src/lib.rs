pub mod rna;
pub mod mrna;

#[cfg(test)]
mod tests {
    use crate::mrna::mRNA;
    use crate::rna::RnaNucleotide;
    use dna::dna::DnaStrand;

    #[test]
    fn produce_mrna() {
        let dna = "TATTAACTGTATTA".to_string();
        let dna : DnaStrand = dna.into();
        let nucs = &*dna.nucleotides().read().unwrap();
        // In real operation, a polymerase will only operate on a slice of the DNA strand
        let nucs = &nucs[5..9];
        let mut rna = mRNA::new();
        nucs.into_iter().for_each(
            |n| {
                rna.add_nucleotide(RnaNucleotide::get_matched_elem(&**n));
            }
        );
        let srna : String = rna.to_string();
        assert_eq!(srna,"UGAC");
    }
}